package com.argus.interview.service;

import com.argus.interview.controller.TestConfig;
import com.argus.interview.exception.MethodNotAllowedException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@Import(TestConfig.class)
public class DataServiceTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DataService dataService;

    private final Map<String, String> map = ImmutableMap.of("hello", "world");


    @Test
    public void sendDataTest() throws JsonProcessingException {
        dataService.setData(map);
        dataService.sendData();

        verify(restTemplate).exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class));
    }

    @Test
    public void getDataGreenTest() {
        dataService.setData(map);
        Map<String, String> result = dataService.getData();

        assertEquals(result, map);
    }

    @Test(expected = MethodNotAllowedException.class)
    public void getDataNullDataTest() {
        dataService.setData(null);
        dataService.getData();
    }
}