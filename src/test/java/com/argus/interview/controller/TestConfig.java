package com.argus.interview.controller;

import com.argus.interview.service.DataService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

@Configuration
public class TestConfig {

    @Bean
    public DataService dataService(){
        return spy(new DataService());
    }

    @Bean(name = "url")
    public String url(){
        return "https://www.google.com";
    }

    @Bean
    public RestTemplate restTemplate(){
        return mock(RestTemplate.class);
    }

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }
}
