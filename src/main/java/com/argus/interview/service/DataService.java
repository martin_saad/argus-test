package com.argus.interview.service;

import com.argus.interview.exception.MethodNotAllowedException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
@Slf4j
public class DataService {

    @Setter
    private Map<String, String> data;

    @Value("${url}")
    private String url;

    @Value("${USERNAME}")
    private String username;

    @Value("${PASSWORD}")
    private String password;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    private final static String PATH = "/v1/data";


    public void sendData() throws JsonProcessingException {
        HttpEntity<?> request = new HttpEntity<>(objectMapper.writeValueAsString(data), createHeaders());
        log.info("sending data {} to {}", data.toString(), url);
        restTemplate.exchange(url + PATH, HttpMethod.POST, request, String.class);
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Basic " + encode(username, password));
        return headers;
    }

    public static String encode(String username, String password){
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.UTF_8));
        return new String(encodedAuth, Charset.defaultCharset());
    }

    public Map<String, String> getData(){
        if (data == null){
            throw new MethodNotAllowedException("Nothing to show. Use POST before trying to GET");
        }
        return data;
    }
}
