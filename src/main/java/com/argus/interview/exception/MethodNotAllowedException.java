package com.argus.interview.exception;

public class MethodNotAllowedException extends RuntimeException {
    public MethodNotAllowedException(String message){
        super(message);
    }
}
