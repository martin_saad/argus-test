package com.argus.interview.controller;

import com.argus.interview.service.DataService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@Slf4j
public class DataController {

    @Autowired
    @Setter
    private DataService dataService;

    @RequestMapping(
            value = "/v1/process", method = RequestMethod.POST)
    public ResponseEntity getData(@RequestBody Map<String, String> payload) throws JsonProcessingException {
        log.info("received POST request with {}", payload);
        dataService.setData(payload);
        dataService.sendData();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @RequestMapping(
            value = "/v1/data", method = RequestMethod.POST)
    public ResponseEntity process(@RequestBody Map<String, String> payload) {
        log.info("received payload {} form different instance", payload);
        dataService.setData(payload);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @RequestMapping(
            value = "/v1/process", method = RequestMethod.GET)
    public ResponseEntity response() {
        Map<String, String> payload = dataService.getData();
        log.info("received GET request, returning payload {}", payload);
        return ResponseEntity.status(HttpStatus.OK).body(payload);
    }
}
