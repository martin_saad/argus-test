package com.argus.interview.controller;

import com.argus.interview.exception.MethodNotAllowedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ExceptionCather {

    @ExceptionHandler(MethodNotAllowedException.class)
    private ResponseEntity handle(MethodNotAllowedException e){
        log.error("error while trying to GET: {}" ,e.getMessage());
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    private ResponseEntity handle(HttpRequestMethodNotSupportedException e){
        log.error("error while trying to GET: {}" ,e.getMessage());
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(e.getMessage());
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    private ResponseEntity handle(HttpMediaTypeNotSupportedException e){
        log.error("error while trying to GET: {}" ,e.getMessage());
        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(e.getMessage());
    }

}
